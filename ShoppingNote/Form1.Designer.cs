﻿namespace ShoppingNote
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.shoppingDataGrid = new System.Windows.Forms.DataGridView();
            this.品名DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.個数DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.メモ欄DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.shoppingDataTableBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.shoppingDataSet = new ShoppingNote.ShoppingDataSet();
            this.goodsName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.addButton = new System.Windows.Forms.Button();
            this.removeButton = new System.Windows.Forms.Button();
            this.quantity = new System.Windows.Forms.MaskedTextBox();
            this.memo = new System.Windows.Forms.TextBox();
            this.shoppingDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.inputButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.shoppingDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoppingDataTableBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoppingDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoppingDataSetBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // shoppingDataGrid
            // 
            this.shoppingDataGrid.AutoGenerateColumns = false;
            this.shoppingDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.shoppingDataGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.品名DataGridViewTextBoxColumn,
            this.個数DataGridViewTextBoxColumn,
            this.メモ欄DataGridViewTextBoxColumn});
            this.shoppingDataGrid.DataSource = this.shoppingDataTableBindingSource;
            this.shoppingDataGrid.Location = new System.Drawing.Point(83, 27);
            this.shoppingDataGrid.Name = "shoppingDataGrid";
            this.shoppingDataGrid.RowTemplate.Height = 21;
            this.shoppingDataGrid.Size = new System.Drawing.Size(344, 150);
            this.shoppingDataGrid.TabIndex = 0;
            // 
            // 品名DataGridViewTextBoxColumn
            // 
            this.品名DataGridViewTextBoxColumn.DataPropertyName = "品名";
            this.品名DataGridViewTextBoxColumn.HeaderText = "品名";
            this.品名DataGridViewTextBoxColumn.Name = "品名DataGridViewTextBoxColumn";
            // 
            // 個数DataGridViewTextBoxColumn
            // 
            this.個数DataGridViewTextBoxColumn.DataPropertyName = "個数";
            this.個数DataGridViewTextBoxColumn.HeaderText = "個数";
            this.個数DataGridViewTextBoxColumn.Name = "個数DataGridViewTextBoxColumn";
            // 
            // メモ欄DataGridViewTextBoxColumn
            // 
            this.メモ欄DataGridViewTextBoxColumn.DataPropertyName = "メモ欄";
            this.メモ欄DataGridViewTextBoxColumn.HeaderText = "メモ欄";
            this.メモ欄DataGridViewTextBoxColumn.Name = "メモ欄DataGridViewTextBoxColumn";
            // 
            // shoppingDataTableBindingSource
            // 
            this.shoppingDataTableBindingSource.DataMember = "ShoppingDataTable";
            this.shoppingDataTableBindingSource.DataSource = this.shoppingDataSet;
            // 
            // shoppingDataSet
            // 
            this.shoppingDataSet.DataSetName = "ShoppingDataSet";
            this.shoppingDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // goodsName
            // 
            this.goodsName.Location = new System.Drawing.Point(168, 230);
            this.goodsName.Name = "goodsName";
            this.goodsName.Size = new System.Drawing.Size(100, 19);
            this.goodsName.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(81, 230);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "品名";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(81, 290);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "個数";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 354);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(34, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "メモ欄";
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(318, 230);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(109, 57);
            this.addButton.TabIndex = 7;
            this.addButton.Text = "メモを追加";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.AddButtonClicked);
            // 
            // removeButton
            // 
            this.removeButton.Location = new System.Drawing.Point(318, 314);
            this.removeButton.Name = "removeButton";
            this.removeButton.Size = new System.Drawing.Size(109, 56);
            this.removeButton.TabIndex = 8;
            this.removeButton.Text = "メモを削除";
            this.removeButton.UseVisualStyleBackColor = true;
            this.removeButton.Click += new System.EventHandler(this.RemoveButtonClicked);
            // 
            // quantity
            // 
            this.quantity.Location = new System.Drawing.Point(168, 287);
            this.quantity.Mask = "000";
            this.quantity.Name = "quantity";
            this.quantity.Size = new System.Drawing.Size(100, 19);
            this.quantity.TabIndex = 9;
            // 
            // memo
            // 
            this.memo.Location = new System.Drawing.Point(168, 354);
            this.memo.Name = "memo";
            this.memo.Size = new System.Drawing.Size(100, 19);
            this.memo.TabIndex = 10;
            // 
            // shoppingDataSetBindingSource
            // 
            this.shoppingDataSetBindingSource.DataSource = this.shoppingDataSet;
            this.shoppingDataSetBindingSource.Position = 0;
            // 
            // inputButton
            // 
            this.inputButton.Location = new System.Drawing.Point(438, 385);
            this.inputButton.Name = "inputButton";
            this.inputButton.Size = new System.Drawing.Size(75, 23);
            this.inputButton.TabIndex = 11;
            this.inputButton.Text = "メモを出力";
            this.inputButton.UseVisualStyleBackColor = true;
            this.inputButton.Click += new System.EventHandler(this.InputMemo);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(525, 450);
            this.Controls.Add(this.inputButton);
            this.Controls.Add(this.memo);
            this.Controls.Add(this.quantity);
            this.Controls.Add(this.removeButton);
            this.Controls.Add(this.addButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.goodsName);
            this.Controls.Add(this.shoppingDataGrid);
            this.Name = "Form1";
            this.Text = "買い物メモ";
            ((System.ComponentModel.ISupportInitialize)(this.shoppingDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoppingDataTableBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoppingDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoppingDataSetBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView shoppingDataGrid;
        private System.Windows.Forms.TextBox goodsName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Button removeButton;
        private System.Windows.Forms.MaskedTextBox quantity;
        private System.Windows.Forms.TextBox memo;
        private System.Windows.Forms.BindingSource shoppingDataTableBindingSource;
        private ShoppingDataSet shoppingDataSet;
        private System.Windows.Forms.BindingSource shoppingDataSetBindingSource;
        private System.Windows.Forms.Button inputButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn 品名DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn 個数DataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn メモ欄DataGridViewTextBoxColumn;
    }
}

