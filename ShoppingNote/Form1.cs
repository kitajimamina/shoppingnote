﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShoppingNote
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void AddButtonClicked(object sender, EventArgs e)
        {
            //DataTableにデータを追加する
            shoppingDataSet.shoppingDataTable.AddshoppingDataTableRow(
                this.goodsName.Text,
                int.Parse(this.quantity.Text),
                this.memo.Text
                );
        }

        private void RemoveButtonClicked(object sender, EventArgs e)
        {
            //選択した行のデータを削除する
            int row = this.shoppingDataGrid.CurrentRow.Index;
            this.shoppingDataGrid.Rows.RemoveAt(row);
        }

        private void InputMemo(object sender, EventArgs e)
        {
            //文字コードを指定
            Encoding enc = Encoding.GetEncoding("UTF-8");

            //ファイルを開く
            StreamWriter writer = new StreamWriter
                    (@"C:\Users\190416PM\Documents\C#\ShoppingNote\ShoppingNote\Note\outputedNote.txt");

            //テキストを書き込む
            var shoppingDataList = shoppingDataSet.shoppingDataTable.ToList();

            foreach(var shoppingData in shoppingDataList)
            {
                //「品名,個数,メモ;」の文字列を生成
                string Record = shoppingData.品名 + "," + shoppingData.個数 + "," + shoppingData.メモ欄 + ";";
                //生成した文字列をファイルに書き込み
                writer.WriteLine(Record);
            }



            //ファイルを閉じる
            writer.Close();

            //書き込み完了のダイアログボックスを呼び出す
            Dialog dialog = new Dialog();
            dialog.Show();
        }
    }
}
